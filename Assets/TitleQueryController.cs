﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleQueryController : MonoBehaviour {
	[SerializeField] private GameObject[] StageSelector;
	private int CurrentSelector;
	private Animator MyAnimator;
	[SerializeField] private FadeOutController FadeOuter;

	[SerializeField] private AudioSource SoundEffectSource;
	[SerializeField] private AudioClip SelectSound;
	[SerializeField] private AudioClip EnterSound;

	[SerializeField] private GameObject[] OrangeSprite;

	private QuerySDEmotionalController EmotionController;
	private bool CanMove = true;

	[SerializeField] private GameObject Boxes;
	private Vector3 BoxOrigin;

	[SerializeField] private SpriteRenderer[] Sprites;
	private float LastKeyTime;
	private bool NeverPressedKey = true;
	void Start(){
		MyAnimator = GetComponentInChildren<Animator>();
		SoundEffectSource.volume = 2;
		EmotionController = GetComponent<QuerySDEmotionalController>();

		BoxOrigin = Boxes.transform.position;
		for(int i=0; i<5; i++){
			
			int isCleared = PlayerPrefs.GetInt(i.ToString(), 0);
			Debug.Log(i + ": " + isCleared);
			if(isCleared > 0){
				OrangeSprite[i].GetComponent<SpriteRenderer>().color = new Color(1,1,1,1);
				if(i != 0){
					float time = PlayerPrefs.GetFloat(i.ToString() + "t", 0);

				}
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(!CanMove) return;
		if(Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow)){//左
			SetLastKeyTime();
			if(CurrentSelector > 0){
				CurrentSelector--;
				Move();
			}
			
		}else if(Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow)){//右
			SetLastKeyTime();
			if(CurrentSelector < StageSelector.Length - 1){
				CurrentSelector++;
				Move();
			}
		}

		if(Input.GetKeyDown(KeyCode.Space)){
			SetLastKeyTime();
			StartCoroutine(DecideAnimation());
		}

		if(Time.timeSinceLevelLoad > 3 && NeverPressedKey){
			for(int i=0; i<Sprites.Length; i++){
				Sprites[i].enabled = true;
			}
		}
	}
	private void SetLastKeyTime(){
		LastKeyTime = Time.timeSinceLevelLoad;
		if(NeverPressedKey)
			NeverPressedKey = false;

	}
	private void Move(){
		SoundEffectSource.PlayOneShot(SelectSound);
		Boxes.transform.position = BoxOrigin + new Vector3(-3.5f * CurrentSelector, 0, 0);
		// transform.position = StageSelector[CurrentSelector].transform.position + new Vector3(-0.2f, 1, -1);
	}
	private IEnumerator DecideAnimation(){
		CanMove = false;
		EmotionController.ChangeEmotion(QuerySDEmotionalController.QueryChanSDEmotionalType.NORMAL_SMILE);
		MyAnimator.SetTrigger("Glad0");
		SoundEffectSource.PlayOneShot(EnterSound);
		yield return new WaitForSeconds(3f);
		FadeOuter.StartFadingOut();
		yield return new WaitForSeconds(1f);
		SceneManager.LoadScene("Stage" + CurrentSelector.ToString());

	}
}
