﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowsController : MonoBehaviour {
	private readonly float ChangeOffset = 1f;
	private float LastChangedTime;
	private SpriteRenderer[] Arrows;
	private int ArrowsIndex;

	void Start(){
		Arrows = GetComponentsInChildren<SpriteRenderer>();
		for(int i=0; i<Arrows.Length; i++){
			Arrows[i].enabled = false;
		}
		Arrows[0].enabled = true;
	}
	
	// Update is called once per frame
	void Update () {
		if(Time.timeSinceLevelLoad - LastChangedTime > ChangeOffset){
			Arrows[ArrowsIndex].enabled = false;

			ArrowsIndex++;
			if(ArrowsIndex > Arrows.Length - 1)
				ArrowsIndex = 0;

			Arrows[ArrowsIndex].enabled = true;
			
			LastChangedTime = Time.timeSinceLevelLoad;
		}
	}
}
