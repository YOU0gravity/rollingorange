﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
	[SerializeField] private GameObject Player;
	private PlayerController PlayerControllerInstance;
	private readonly float OffsetX = 2f;
	private float CameraRightLimit;
	[SerializeField] private GameManager GMInstance;


	private bool IsGameEnd;


	private Camera MyCamera;
	private bool IsZooming;
	private readonly float ZoomSizeLimit = 7;
	private float ZoomSize = 10.5f;
	private bool IsLerping;
	private Vector3 LerpStartPos;
	private Vector3 LerpEndPos;
	private float LerpStartTime;


	// Use this for initialization
	void Start () {
		PlayerControllerInstance = Player.GetComponent<PlayerController>();

		MyCamera = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void LateUpdate () {
		if(!IsGameEnd){
			Vector3 playerPos = Player.transform.position;
			Vector3 newCameraPos = transform.position;

			if(newCameraPos.x - playerPos.x > OffsetX){
				newCameraPos.x = playerPos.x + OffsetX;
			}else if(newCameraPos.x - playerPos.x < -OffsetX){
				newCameraPos.x = playerPos.x - OffsetX;
			}


			if(newCameraPos.x < 0){
				newCameraPos.x = 0;
			}else if(newCameraPos.x > CameraRightLimit){
				newCameraPos.x = CameraRightLimit;
			}


			transform.position = new Vector3(newCameraPos.x, newCameraPos.y, -10);
		}

		if(IsZooming){
			ZoomSize -= Time.deltaTime * 2;
			if(ZoomSize < ZoomSizeLimit){
				ZoomSize = ZoomSizeLimit;
				IsZooming = false;
			}
			MyCamera.orthographicSize = ZoomSize;
		}
		if(IsLerping){
			var diff = Time.timeSinceLevelLoad - LerpStartTime;
			if(diff > 1){
				IsLerping = false;
				transform.position = LerpEndPos;
				return;
			}
			transform.position = Vector3.Lerp(LerpStartPos, LerpEndPos, diff);
		}
		
	}

	public void SetRightEnd(float num){
		CameraRightLimit = num * 32;
	}
	public void ZoomIntoPlayer(){
		IsZooming = true;
		IsLerping = true;
		IsGameEnd = true;

		LerpStartTime = Time.timeSinceLevelLoad;
		LerpStartPos = transform.position;
		float posX = Player.transform.position.x;
		LerpEndPos = new Vector3(posX, 9.5f, -10);
	}
}
