﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {
	[SerializeField] private float MoveSpeed = 2;
	[SerializeField] private int MoveType = -1;
	[SerializeField] private float PlusMoveLimit;
	[SerializeField] private float MinusMoveLimit;
	private Vector3 OriginPos;
	private float CurrentXOffset;
	private float CurrentYOffset;
	private bool IsPlusDirection;

	// Use this for initialization
	void Start () {
		OriginPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if(MoveType != -1){
			switch(MoveType){
				case 0://上下
					if(IsPlusDirection){
						CurrentYOffset += Time.deltaTime * MoveSpeed;
						if(CurrentYOffset > PlusMoveLimit){
							CurrentYOffset = PlusMoveLimit;
							IsPlusDirection = false;
						}
					}else{
						CurrentYOffset -= Time.deltaTime * MoveSpeed;
						if(CurrentYOffset < MinusMoveLimit){
							CurrentYOffset = MinusMoveLimit;
							IsPlusDirection = true;
						}
					}
				break;
				case 1://左右
					if(IsPlusDirection){
						CurrentXOffset += Time.deltaTime * MoveSpeed;
						if(CurrentXOffset > PlusMoveLimit){
							CurrentXOffset = PlusMoveLimit;
							IsPlusDirection = false;
						}
					}else{
						CurrentXOffset -= Time.deltaTime * MoveSpeed;
						if(CurrentXOffset < MinusMoveLimit){
							CurrentYOffset = MinusMoveLimit;
							IsPlusDirection = true;
						}
					}
				break;

			}
			transform.position = OriginPos + new Vector3(CurrentXOffset, CurrentYOffset, 0);
		}
	}

	public void OnTriggerEnter(Collider other){
		if(other.gameObject.tag == "Player"){
			other.gameObject.GetComponent<PlayerController>().StartBeingDamaged();
		}
	}
}
