﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunRotater : MonoBehaviour {
	private bool IsExpanding;
	private float CurrentScale;


	// Use this for initialization
	void Start () {
		IsExpanding = true;
		CurrentScale = 1;
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(0,0,5 * Time.deltaTime);
		if(IsExpanding){
			CurrentScale += 0.1f * Time.deltaTime;
			if(CurrentScale > 1.1f)
				IsExpanding = false;
		}else{
			CurrentScale -= 0.1f * Time.deltaTime;
			if(CurrentScale < 0.9f)
				IsExpanding = true;
		}
		transform.localScale = Vector3.one * CurrentScale;
	}
}
