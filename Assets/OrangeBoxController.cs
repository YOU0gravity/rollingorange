using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrangeBoxController : MonoBehaviour {
    private bool IsDuringJumping;
	private bool IsMoveUpside;
    private readonly float UpLimit = 0.5f;
    private readonly float DownLimit = 0;
    private Vector3 OriginLocalPos;
    private float CurrentPosY;
    private readonly float JumpSpeed = 6f;
    [SerializeField] private GameObject[] BoxIllusts;

    [SerializeField] private ParticleSystem TwinkleParticle0;
    [SerializeField] private ParticleSystem TwinkleParticle1;


	void Start(){
        OriginLocalPos = transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
        if(IsDuringJumping){
            if(IsMoveUpside){
                CurrentPosY += Time.deltaTime * JumpSpeed;
                if(transform.localPosition.y > UpLimit){
                    CurrentPosY = UpLimit;
                    IsMoveUpside = false;
                }
            }else{
                CurrentPosY -= Time.deltaTime * JumpSpeed;
                if(transform.localPosition.y < DownLimit){
                    CurrentPosY = DownLimit;
                    IsDuringJumping = false;
                }
            }
            OriginLocalPos.y = CurrentPosY;
            transform.localPosition = OriginLocalPos;
        }
	}

    public void StartJumping(){
        IsDuringJumping = true;
        IsMoveUpside = true;
    }

    public void ChangeIllust(int orangeCount){
        TwinkleParticle0.Emit(10);
		TwinkleParticle1.Emit(10);

        // int digit1 = orangeCount % 10;
        // bool[] activeInfo = new bool[]{false, false};
        // switch(digit1){
        //     case 0: case 5:
        //         StartCoroutine(CreateNewBox(orangeCount));
        //         break;
        //     case 3: case 4: case 8: case 9:
        //         activeInfo[1] = true;
        //         break;
        //     default:
        //         activeInfo[0] = true;
        //         break;
        // }
        // for(int i=0; i<2; i++){
        //     BoxIllusts[i].SetActive(activeInfo[i]);
        // }
        BoxIllusts[1].SetActive(false);
        BoxIllusts[2].SetActive(true);
    }
    private IEnumerator CreateNewBox(int orangeCount){
        Vector3 BoxPos = BoxIllusts[2].transform.position;
        GameObject newBox = Instantiate(BoxIllusts[2], BoxPos, Quaternion.identity);
        newBox.SetActive(true);

        int boxCount = orangeCount / 5;
        newBox.GetComponent<FilledBoxController>().SetPos(newBox.transform.position, new Vector3(BoxPos.x + 6, BoxPos.y + 3 * boxCount, 0));
        yield return new WaitForSeconds(1f);
        BoxIllusts[0].SetActive(true);
    }
}