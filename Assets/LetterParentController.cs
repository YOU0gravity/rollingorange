﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetterParentController : MonoBehaviour {
	private LetterController[] LCInstances;


	private IEnumerator GeneratingCoroutine(){
		for(int i=0; i<LCInstances.Length; i++){
			LCInstances[i].StartGenerating();
			yield return new WaitForSeconds(0.2f);
		}
	}
	public void ActiveScript(){
		gameObject.SetActive(true);
		LCInstances = GetComponentsInChildren<LetterController>();
		StartCoroutine(GeneratingCoroutine());
	}
}
