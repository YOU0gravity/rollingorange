﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessageBoardController : MonoBehaviour {
	private int MessageNum;
	[SerializeField] private GameObject[] MessageBoards;
	[SerializeField] private SpriteRenderer[] PressSpaceMessages;
	private float LastPressedSpaceKeyTime;
	void Update(){
		if(Input.GetKeyDown(KeyCode.Space)){
			if(MessageNum < MessageBoards.Length - 1){
				LastPressedSpaceKeyTime = Time.timeSinceLevelLoad;
				MessageBoards[MessageNum].SetActive(false);
				MessageNum++;
				MessageBoards[MessageNum].SetActive(true);

				for(int i=0; i<PressSpaceMessages.Length; i++)
					PressSpaceMessages[i].enabled = false;

			}else{
				Destroy(gameObject);
			}
		}
		if(Time.timeSinceLevelLoad - LastPressedSpaceKeyTime > 2){
			for(int i=0; i<PressSpaceMessages.Length; i++)
				PressSpaceMessages[i].enabled = true;
		}
	}

	
}
