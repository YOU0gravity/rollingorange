﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
	public int StageNum = 0;
	[SerializeField] private GameObject Player;
	private PlayerController PlayerControllerInstance;
	private QuerySDEmotionalController EmotionController;
	[SerializeField] private GameObject OrangeSpherePrefab;
	[SerializeField] private Sprite[] NumberSpriteResourses;
	[SerializeField] private SpriteRenderer[] TimeSpriteRenderer;
	private int GotOrangeCount;
	private Vector3 GeneratePos = new Vector3(-10, 11, 0);
	private GameObject Orange;
	[System.NonSerializedAttribute] public GameObject RollingOrange;

	
	[SerializeField] private float LeftTime = 120;
	private float GameTimeProgress;
	[SerializeField] private GameObject TimeBoard;


	[SerializeField] private GameObject BoxPrefab;
	private OrangeBoxController OrangeBoxControllerInstance;


	private int ScrollCount;
	[SerializeField] private GameObject AddedGroundPrefab;
	[SerializeField] private GameObject LastGroundPrefab;
	private List<GameObject> AddedGroundList;


	[SerializeField] private CameraController CCInstance;

	[SerializeField] private FadeOutController FadeOutScript;

	public int MaxGroundNum = 3;
	//1: 32
	//2: 64 + 16
	//3: 96


	private float LastFallingTime;
	private bool IsDuringCoroutine;

	private bool IsGameEnd;
	[SerializeField] private GameObject TreeForHide;


	[SerializeField] private LetterParentController GameEndEffect;
	[SerializeField] private LetterParentController GameOverEffect;


	[SerializeField] private bool IsTutorial;
	[SerializeField] private GameObject InstructionMessage;


	[SerializeField] private AudioSource SoundEffectSource;
	[SerializeField] private AudioSource BGMSource;
	[SerializeField] private AudioClip TreeSound;
	[SerializeField] private AudioClip OrangeLandingSound;
	[SerializeField] private AudioClip EnemyCollideSound;
	[SerializeField] private AudioClip OrangeGotSound;
	[SerializeField] private AudioClip SelectSound;
	[SerializeField] private AudioClip StageCompleteSound;
	[SerializeField] private AudioClip GameOverSound;
	// Use this for initialization
	void Start () {
		PlayerControllerInstance = Player.GetComponent<PlayerController>();
		EmotionController = GetComponent<QuerySDEmotionalController>();

		Orange = Instantiate(OrangeSpherePrefab, GeneratePos, Quaternion.identity) as GameObject;
		

		AddedGroundList = new List<GameObject>();

		CCInstance.SetRightEnd(MaxGroundNum);
		PlayerControllerInstance.SetRightEnd(MaxGroundNum);
		
		GameObject box = Instantiate(BoxPrefab, new Vector3(MaxGroundNum * 32 + 10, 0.8f, 0), Quaternion.identity);
		OrangeBoxControllerInstance = box.GetComponentInChildren<OrangeBoxController>();

		if(IsTutorial){
			InstructionMessage.SetActive(true);
			TimeBoard.SetActive(false);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(!IsGameEnd){
			GameTimeProgress += Time.deltaTime;
			float viewTime = LeftTime - GameTimeProgress;
			if(viewTime > 0){
				SetTimeToBoard(viewTime);
			}else{
				StartCoroutine(TimeUpCoroutine());
			}
		}

		float OffsetPosX = 31 * ScrollCount - 15;
		if(Player.transform.position.x > OffsetPosX){
			if(ScrollCount < MaxGroundNum){
				if(RollingOrange != null){
					ScrollCount++;
					if(TreeForHide != null)
						Destroy(TreeForHide);

					GameObject added = Instantiate(AddedGroundPrefab, new Vector3(32 * ScrollCount, 9.6f, 0), Quaternion.identity);
					AddedGroundList.Add(added);
					
					if(AddedGroundList.Count > 4){
						Destroy(AddedGroundList[0]);
						AddedGroundList.RemoveAt(0);
					}
					
				}else{
					//TODO: みかんを転がそう！メッセージをのせる

				}
				CCInstance.SetRightEnd(ScrollCount);
				PlayerControllerInstance.SetRightEnd(ScrollCount);
			}else{
				if(RollingOrange != null){
					ScrollCount++;
					Instantiate(LastGroundPrefab, new Vector3(32 * ScrollCount, 9.6f, 0), Quaternion.identity);
				}
			}
		}

	}

	private IEnumerator StartOrangeSwingingAndFalling(){
		IsDuringCoroutine = true;
		yield return new WaitForSeconds(0.5f);
		OrangeController oc = Orange.GetComponent<OrangeController>();
		oc.StartSwinging();
		yield return new WaitForSeconds(1f);
		RollingOrange = Orange;
		oc.StartFalling();
		yield return new WaitForSeconds(1f);
		Orange = null;
		IsDuringCoroutine = false;

	}

	private void SetTimeToBoard(float timeProgress){
		int timeProgressInt = (int)timeProgress;
		int min = timeProgressInt / 60;
		int sec = timeProgressInt % 60;

		int[] digits = new int[4];
		digits[0] = min / 10;
		digits[1] = min - digits[0] * 10;
		digits[2] = sec / 10;
		digits[3] = sec - digits[2] * 10;

		for(int i=0; i<4; i++)
			TimeSpriteRenderer[i].sprite = NumberSpriteResourses[digits[i]];
	}

	public void GetOrange(){
		SoundEffectSource.PlayOneShot(OrangeGotSound);

		GotOrangeCount++;
		OrangeBoxControllerInstance.StartJumping();

		OrangeBoxControllerInstance.ChangeIllust(GotOrangeCount);

		StartCoroutine(GameClearCoroutine());
	}

	private IEnumerator TimeUpCoroutine(){
		IsGameEnd = true;
		BGMSource.Stop();
		yield return new WaitForSeconds(1f);

		SoundEffectSource.PlayOneShot(GameOverSound);
		SoundEffectSource.volume = 0.3f;
		GameOverEffect.ActiveScript();
		PlayerControllerInstance.StartBeingSad();
		CCInstance.ZoomIntoPlayer();

		yield return new WaitForSeconds(4f);
		FadeOutScript.StartFadingOut();
		yield return new WaitForSeconds(2f);
		SceneManager.LoadScene("Title");

	}
	
	private IEnumerator GameClearCoroutine(){
		PlayerPrefs.SetInt(StageNum.ToString(), 1);
		if(StageNum != 0){
			PlayerPrefs.SetFloat(StageNum.ToString() + "t", GameTimeProgress);
		}
		IsGameEnd = true;
		BGMSource.Stop();
		yield return new WaitForSeconds(1f);
		SoundEffectSource.PlayOneShot(StageCompleteSound);
		SoundEffectSource.volume = 0.3f;
		GameEndEffect.ActiveScript();
		PlayerControllerInstance.StartGladAnimation();
		CCInstance.ZoomIntoPlayer();
		yield return new WaitForSeconds(4f);
		FadeOutScript.StartFadingOut();
		yield return new WaitForSeconds(2f);
		SceneManager.LoadScene("Title");

	}

	public void RegenerateOrange(){
		if(Orange != null) return;

		SoundEffectSource.PlayOneShot(OrangeLandingSound);

		if(ScrollCount <= 1){
			GeneratePos = new Vector3(-10, 11, 0);
		}else{
			if(MaxGroundNum != 0){
				if(ScrollCount >= MaxGroundNum + 2){
					//右リミット限界までいってしまっても、右リミットの木より１つ前のところに再生成されるようにする
					GeneratePos = new Vector3(32 * (ScrollCount-1) - 48, 11, 0);
				}else{
					GeneratePos = new Vector3(32 * ScrollCount - 48, 11, 0);
				}
				
			}else{
				//MaxGroundNumが0の場合、最初の木でしか生成されない
				GeneratePos = new Vector3(-10, 11, 0);
			}
		}
		
		Orange = Instantiate(OrangeSpherePrefab, GeneratePos, Quaternion.identity) as GameObject;

		PlayerControllerInstance.StartBeingDisappointed();
	}

	public bool FallOrange(){
		if(RollingOrange != null) return false;
		if(IsGameEnd) return false;

		if(Time.timeSinceLevelLoad - LastFallingTime > 2f){
			SoundEffectSource.PlayOneShot(TreeSound);

			if(Orange != null && !IsDuringCoroutine)
				StartCoroutine(StartOrangeSwingingAndFalling());
			
			LastFallingTime = Time.timeSinceLevelLoad;
			return true;
		}
		return false;
	}

	public void EnemyCollide(){
		SoundEffectSource.PlayOneShot(EnemyCollideSound);
	}

}