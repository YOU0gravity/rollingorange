﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrangeController : MonoBehaviour {
	private Rigidbody MyRigidBody;
	[SerializeField] private CapsuleCollider MyCollider;
	[SerializeField] private CapsuleCollider MyTrigger;

	private bool IsSwinging;
	private bool IsSwingingTowardsRight;
	private float CurrentSwingAngle;
	private readonly float SwingAngleLimit = 20;
	private readonly float SwingSpeed = 300;

	private bool IsGenerating;
	private bool IsDissapearing;
	private float Scale = 0.1f;



	public bool IsOnTree = true;


	void Awake(){
		IsGenerating = true;
	}
	// Use this for initialization
	void Start () {
		// MyCollider = GetComponent<CapsuleCollider>();
		MyRigidBody = GetComponent<Rigidbody>();

		MyCollider.enabled = false;
		MyRigidBody.constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY;
	}
	
	// Update is called once per frame
	void Update () {
		if(IsGenerating){
			Scale += Time.deltaTime;
			if(Scale > 1.5f){
				Scale = 1.5f;
				IsGenerating = false;
			}
			transform.localScale = Vector3.one * Scale;
		}

		if(IsDissapearing){
			Scale -= Time.deltaTime;
			transform.localScale = Vector3.one * Scale;
			if(Scale < 0.1f){
				Destroy(gameObject);
			}
		}

		if(IsSwinging){

			if(IsSwingingTowardsRight){
				CurrentSwingAngle += Time.deltaTime * SwingSpeed;
			}else{
				CurrentSwingAngle -= Time.deltaTime * SwingSpeed;
			}

			if(Mathf.Abs(CurrentSwingAngle) > SwingAngleLimit){
				if(IsSwingingTowardsRight){
					CurrentSwingAngle = SwingAngleLimit;
				}else{
					CurrentSwingAngle = -SwingAngleLimit;
				}
				IsSwingingTowardsRight = !IsSwingingTowardsRight;
			}

			transform.rotation = Quaternion.Euler(0,0,CurrentSwingAngle);
		}
	}

	public void StartSwinging(){
		IsSwinging = true;
	}
	public void StartFalling(){
		IsOnTree = false;
		IsSwinging = false;
		MyCollider.enabled = true;
		MyTrigger.enabled = false;
		MyRigidBody.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY;
	}

	public void OnLandingGround(){
		IsDissapearing = true;
	}
}