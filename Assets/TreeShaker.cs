﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeShaker : MonoBehaviour {

	private bool IsTreeShaking;
	private bool IsTreeShakingTowardRight;
	private float ShakingTreeAngle;
	private readonly float TreeAngleLimit = 1.5f;
	private float TreeShakingStartTime;

	private GameManager GMInstance;

	[SerializeField] private bool HasOrange;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(IsTreeShaking){

			if(IsTreeShakingTowardRight){
				ShakingTreeAngle += Time.deltaTime * 25;
			}else{
				ShakingTreeAngle -= Time.deltaTime * 25;
			}

			if(Mathf.Abs(ShakingTreeAngle) > TreeAngleLimit){
				if(IsTreeShakingTowardRight){
					ShakingTreeAngle = TreeAngleLimit;
				}else{
					ShakingTreeAngle = -TreeAngleLimit;
				}
				IsTreeShakingTowardRight = !IsTreeShakingTowardRight;
			}

			transform.rotation = Quaternion.Euler(0,0,ShakingTreeAngle);

			if(Time.timeSinceLevelLoad - TreeShakingStartTime > 1f){
				IsTreeShaking = false;
			}
		}
	}


	private void StartShaking(){
		bool can = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>().FallOrange();
		if(can){
			StartCoroutine(ShakingCoroutine());
			HasOrange = false;
		}
	}
	private IEnumerator ShakingCoroutine(){
		yield return new WaitForSeconds(0.5f);
		TreeShakingStartTime = Time.timeSinceLevelLoad;
		IsTreeShaking = true;
	}

	public void OnTriggerEnter(Collider other){
		if(other.gameObject.tag == "Orange"){
			if(other.gameObject.GetComponent<OrangeController>().IsOnTree){
				HasOrange = true;
			}
		}
	}
	public void OnTriggerStay(Collider other){
		if(other.gameObject.tag == "Player"){
			if(HasOrange){
				StartShaking();
			}
		}
	}
}
