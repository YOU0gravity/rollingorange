﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetterController : MonoBehaviour {
	private bool IsGenerating;
	private float Scale;
	

	// Use this for initialization
	void Start () {
		Scale = 2;
		transform.localScale = Vector3.zero;

	}
	
	// Update is called once per frame
	void Update () {
		if(IsGenerating){
			Scale -= Time.deltaTime * 3;
			if(Scale < 1f){
				Scale = 1f;
				IsGenerating = false;
			}
			transform.localScale = Vector3.one * Scale;
		}

	}
	public void StartGenerating(){
		IsGenerating = true;
	}
}
