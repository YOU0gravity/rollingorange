﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	private float MoveSpeed = 10;
	private float RunningStartTime;
	[SerializeField] private GameObject PlayerModel;
	private MeshRenderer[] Meshes;
	private SkinnedMeshRenderer[] SkinnedMeshes;

	private Animator MyAnimator;

	private QuerySDEmotionalController EmotionController;

	private float RightLimit = 3000f;
	public float LeftLimit = -17;

	private bool CanMove;
	[SerializeField] private SpriteRenderer MyUmbrellaRenderer;
	private bool IsLerping;
	private Vector3 LerpStartPos;
	private Vector3 LerpEndPos;
	private float LerpStartTime;
	
	private bool BeingDamaged;
	private bool BeingDisappointed;

	public bool IsClearAnimationEnded;

	[SerializeField] GameManager GMScript;

	// Use this for initialization
	void Start () {
		PlayerModel.transform.localRotation = Quaternion.Euler(0, -90, 0);
		MyAnimator = GetComponentInChildren<Animator>();
		EmotionController = GetComponentInChildren<QuerySDEmotionalController>();

		RunningStartTime = -10;

		Meshes = GetComponentsInChildren<MeshRenderer>();
		SkinnedMeshes = GetComponentsInChildren<SkinnedMeshRenderer>();

		for(int i=0; i<Meshes.Length; i++){
			Meshes[i].sortingLayerName = "Orange";
		}
		for(int i=0; i<SkinnedMeshes.Length; i++){
			SkinnedMeshes[i].sortingLayerName = "Orange";
		}
		CanMove = true;
	}
	
	// Update is called once per frame
	void Update () {
		if(CanMove && !BeingDamaged){

			int moveNum = -1;
			if(Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)){//上
				if(transform.position.y < 15){
					moveNum = 0;
				}
			}else if(Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)){//下
				if(transform.position.y > 0){
					moveNum = 2;
				}
			}
			if(Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)){//左
				if(transform.position.x > LeftLimit){
					moveNum = 1;
				}
				
			}else if(Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)){//右
				if(transform.position.x < RightLimit){
					moveNum = 3;
				}
			}

			Move(moveNum);
		}else{

			if(IsLerping){
				var diff = Time.timeSinceLevelLoad - LerpStartTime;
				if(diff > 1){
					IsLerping = false;
					transform.position = LerpEndPos;
					return;
				}
				transform.position = Vector3.Lerp(LerpStartPos, LerpEndPos, diff);
			}

		}

	}

	public void Move(int directionNumber){
		switch(directionNumber){
			case 0:
				transform.Translate(Vector3.up * MoveSpeed * Time.deltaTime);
				break;
			case 1:
				PlayerModel.transform.localRotation = Quaternion.Euler(0, -90, 0);
				transform.Translate(Vector3.left * MoveSpeed * Time.deltaTime);
				break;
			case 2:
				// MecanimController.ChangeAnimation(QuerySDMecanimController.QueryChanSDAnimationType.NORMAL_FLY_DOWN, true);
				transform.Translate(Vector3.down * MoveSpeed * Time.deltaTime);
				break;
			case 3:
				PlayerModel.transform.localRotation = Quaternion.Euler(0, 90, 0);
				transform.Translate(Vector3.right * MoveSpeed * Time.deltaTime);
				break;
		}
	}
	public void SetRightEnd(float num){
		RightLimit = num * 32 + 16;
	}


	public void StartGladAnimation(){
		StartCoroutine(GladAnimationCoroutine());
	}
	private IEnumerator GladAnimationCoroutine(){
		PlayerModel.transform.localRotation = Quaternion.Euler(0, 180, 0);
		MyAnimator.SetTrigger("Glad0");
		MyUmbrellaRenderer.enabled = false;
		EmotionController.ChangeEmotion(QuerySDEmotionalController.QueryChanSDEmotionalType.NORMAL_SMILE);

		CanMove = false;
		IsLerping = true;
		LerpStartTime = Time.timeSinceLevelLoad;
		LerpStartPos = transform.position;
		LerpEndPos = new Vector3(transform.position.x, 7, 0);
		yield return new WaitForSeconds(1f);
	}

	public void StartBeingDamaged(){
		if(!BeingDamaged){
			GMScript.EnemyCollide();
			MyAnimator.SetTrigger("Damaged");
			StartCoroutine(BeingDamagedCoroutine());
		}
	}
	private IEnumerator BeingDamagedCoroutine(){
		BeingDamaged = true;
		EmotionController.ChangeEmotion(QuerySDEmotionalController.QueryChanSDEmotionalType.NORMAL_GURUGURU);
		yield return new WaitForSeconds(0.5f);
		BeingDamaged = false;
		EmotionController.ChangeEmotion(QuerySDEmotionalController.QueryChanSDEmotionalType.NORMAL_DEFAULT);
	}
	public void StartBeingDisappointed(){
		if(!BeingDisappointed){
			MyAnimator.SetTrigger("Disappointed");
			StartCoroutine(BeingDisappointedCoroutine());
		}
	}
	private IEnumerator BeingDisappointedCoroutine(){
		BeingDisappointed = true;
		EmotionController.ChangeEmotion(QuerySDEmotionalController.QueryChanSDEmotionalType.NORMAL_SURPRISE);
		yield return new WaitForSeconds(0.5f);
				EmotionController.ChangeEmotion(QuerySDEmotionalController.QueryChanSDEmotionalType.NORMAL_DEFAULT);
		BeingDisappointed = false;
	}

	public void StartBeingSad(){
		StartCoroutine(SadAnimationCoroutine());
	}
	private IEnumerator SadAnimationCoroutine(){
		PlayerModel.transform.localRotation = Quaternion.Euler(0, 180, 0);
		MyAnimator.SetBool("Sad", true);
		MyUmbrellaRenderer.enabled = false;
		EmotionController.ChangeEmotion(QuerySDEmotionalController.QueryChanSDEmotionalType.NORMAL_SAD);

		CanMove = false;
		IsLerping = true;
		LerpStartTime = Time.timeSinceLevelLoad;
		LerpStartPos = transform.position;
		LerpEndPos = new Vector3(transform.position.x, 7, 0);
		yield return new WaitForSeconds(1f);
	}
}
