﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FilledBoxController : MonoBehaviour {
	private Vector3 StartPos;
	private Vector3 EndPos;
	private float LerpStartedTime;
	private bool IsLerping;


	// Use this for initialization
	void Start () {
		StartPos = transform.position;
	}
	// Update is called once per frame
	void Update () {
		if(IsLerping){
			var diff = Time.timeSinceLevelLoad - LerpStartedTime;
			if(diff > 1){
				transform.position = EndPos;
				IsLerping = false;
			}
			transform.position = Vector3.Lerp(StartPos, EndPos, diff);
		}
	}
	public void SetPos(Vector3 startPos, Vector3 endPos){
		StartPos = startPos;
		EndPos = endPos;
		IsLerping = true;
		LerpStartedTime = Time.timeSinceLevelLoad;
	}
}
