﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeOutController : MonoBehaviour {
	private SpriteRenderer MyRenderer;
	private bool IsFadingOut;
	private bool IsFadingIn;
	private float AlphaValue = 0;

	public bool InitialBlack;

	// Use this for initialization
	void Start () {
		MyRenderer = GetComponent<SpriteRenderer>();
		
		Color c = MyRenderer.color;
		if(InitialBlack){
			c.a = 1;
			IsFadingIn = true;
			AlphaValue = 1;
		}else{
			c.a = 0;
		}
		MyRenderer.color = c;
	}
	
	// Update is called once per frame
	void Update () {
		if(IsFadingOut){
			AlphaValue += Time.deltaTime * 2;
			if(AlphaValue > 1){
				AlphaValue = 1;
				IsFadingOut = false;
			}
			Color c = MyRenderer.color;
			c.a = AlphaValue;
			MyRenderer.color = c;
		}
		if(IsFadingIn){
			AlphaValue -= Time.deltaTime * 2;
			if(AlphaValue < 0){
				AlphaValue = 0;
				IsFadingIn = false;
			}
			Color c = MyRenderer.color;
			c.a = AlphaValue;
			MyRenderer.color = c;
		}
		
	}
	public void StartFadingOut(){
		IsFadingOut = true;	
	}
	public void StartFadingIn(){
		IsFadingIn = true;
	}
}
