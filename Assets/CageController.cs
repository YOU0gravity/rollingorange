﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CageController : MonoBehaviour {
	private GameManager GMInstance;

	// Use this for initialization
	void Start () {
		GMInstance = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter(Collider other){
		if(other.gameObject.tag == "Orange"){
			GMInstance.GetOrange();
			Destroy(other.gameObject);
		}
	}
}
