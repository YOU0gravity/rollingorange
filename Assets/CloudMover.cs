﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudMover : MonoBehaviour {
	[SerializeField] private float MoveSpeed;
	private readonly float EndPosX = -20;
	private readonly float RestartPosX = 20;
	private Vector3 OriginPos;


	// Use this for initialization
	void Start () {
		OriginPos = transform.localPosition;
		OriginPos.x = RestartPosX;
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate(-MoveSpeed * Time.deltaTime, 0, 0);

		if(transform.localPosition.x < EndPosX)
			transform.localPosition = OriginPos;
	}
}
